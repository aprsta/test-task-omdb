package org.example.task.mvvm.model.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SearchEntity(
        @SerializedName("imdbID")
        @Expose
        val identifier: String? = null,

        @SerializedName("Title")
        @Expose
        val title: String? = null
)