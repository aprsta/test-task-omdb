package org.example.task.mvvm.view.activity

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.example.task.R
import org.example.task.common.config.TransmittableKeys
import org.example.task.databinding.ActivityFilmDetailsBinding
import org.example.task.mvvm.observable.viewmodel.FilmDetailsViewModel

class FilmDetailsActivity : AppCompatActivity() {
    private lateinit var viewModel: FilmDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent == null || !intent.hasExtra(TransmittableKeys.IMDB_ID)) {
            // IMDB ID required.
            assert(false)
        }

        val binding = DataBindingUtil.setContentView<ActivityFilmDetailsBinding>(this, R.layout.activity_film_details)
        viewModel = ViewModelProviders.of(this).get(FilmDetailsViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.filmDetailsImdbId = intent.getStringExtra(TransmittableKeys.IMDB_ID)

        viewModel.observeAll(this, findViewById(R.id.filmDetailsSwipeView))

        if (savedInstanceState == null) {
            viewModel.update()
        }
    }
}
