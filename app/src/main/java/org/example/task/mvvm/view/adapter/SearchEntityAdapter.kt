package org.example.task.mvvm.view.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import org.example.task.R
import org.example.task.databinding.ItemSearchEntityBinding
import org.example.task.mvvm.observable.baseobservable.SearchEntityBaseObservable

class SearchEntityAdapter : RecyclerView.Adapter<SearchEntityAdapter.SearchEntityItemViewHolder>() {
    private val items: MutableList<SearchEntityBaseObservable> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchEntityItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate(inflater, R.layout.item_search_entity, parent, false) as ItemSearchEntityBinding
        return SearchEntityItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchEntityItemViewHolder, position: Int) {
        val baseObservable = items[position]
        holder.bind(baseObservable)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun update(data: List<SearchEntityBaseObservable>) {
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }

    class SearchEntityItemViewHolder(val binding: ItemSearchEntityBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(baseObservable: SearchEntityBaseObservable) {
            this.binding.baseObservable = baseObservable
            this.binding.executePendingBindings()
        }
    }
}