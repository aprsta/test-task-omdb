package org.example.task.mvvm.observable.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.databinding.Bindable
import android.databinding.BindingAdapter
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.support.v4.app.SupportActivity
import android.support.v4.widget.SwipeRefreshLayout
import android.widget.ImageView
import org.example.task.Application
import org.example.task.BR
import org.example.task.common.util.ObservableViewModel
import org.example.task.mvvm.model.data.FilmDetails
import org.example.task.mvvm.model.network.NetworkApi
import javax.inject.Inject


class FilmDetailsViewModel : ObservableViewModel() {
    /** Section: Injects. */

    @Inject
    lateinit var networkApi: NetworkApi

    /** Section: Bindable properties. */

    var filmDetailsImdbId: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.filmDetailsImdbId)
        }

    var filmDetailsTitle: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.filmDetailsTitle)
        }

    var filmDetailsPosterUrl: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.filmDetailsPosterUrl)
        }

    var filmDetailsDrawable: Drawable? = null
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.filmDetailsDrawable)
        }

    var filmDetailsRating: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.filmDetailsRating)
        }

    var filmDetailsCountry: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.filmDetailsCountry)
        }

    var filmDetailsDirector: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.filmDetailsDirector)
        }

    var filmDetailsActors: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.filmDetailsActors)
        }

    var filmDetailsPlot: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.filmDetailsPlot)
        }

    var filmDetailsLoading: Boolean = false
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.filmDetailsLoading)
        }

    /** Section: Live data. */

    // TODO: Modify liveData to return error details.
    private val filmDetailsLiveData = MutableLiveData<FilmDetails>()
    private val posterLiveData = MutableLiveData<Bitmap>()

    /** Section: Constructor. */

    init {
        Application.component.inject(this)
    }

    /** Section: Utility methods. */

    fun observeAll(activity: SupportActivity) {
        observeAll(activity, null)
    }

    fun observeAll(activity: SupportActivity, view: SwipeRefreshLayout?) {
        filmDetailsLiveData.observe(activity, Observer {
            if (it == null) {
                filmDetailsLoading = false
                view?.isRefreshing = false
                return@Observer
            }

            filmDetailsTitle = it.title!!
            filmDetailsRating = it.rating!!
            filmDetailsCountry = it.country!!
            filmDetailsDirector = it.director!!
            filmDetailsActors = it.actors!!
            filmDetailsPlot = it.plot!!
            filmDetailsPosterUrl = it.posterUrl!!

            if (!filmDetailsPosterUrl.isEmpty()) {
                networkApi.asyncDownloadPoster(filmDetailsPosterUrl, posterLiveData)
            } else {
                filmDetailsLoading = false
                view?.isRefreshing = false
            }
        })

        posterLiveData.observe(activity, Observer {
            filmDetailsLoading = false
            view?.isRefreshing = false

            if (it == null) {
                return@Observer
            }

            filmDetailsDrawable = BitmapDrawable(activity.resources, it)
        })
    }

    /** Section: Public methods. */

    fun update() {
        if (filmDetailsImdbId.isEmpty()) {
            return
        }

        filmDetailsLoading = true

        networkApi.asyncFilmDetails(filmDetailsImdbId, filmDetailsLiveData)
    }

    /** Section: Binding adapters. */

    companion object {
        @BindingAdapter("app:srcCompat")
        @JvmStatic
        fun bindSrcCompat(imageView: ImageView, drawable: Drawable?) {
            if (drawable == null) {
                return
            }

            imageView.setImageDrawable(drawable)
        }
    }
}