package org.example.task.mvvm.model.network

import okhttp3.ResponseBody
import org.example.task.common.config.ApiConfig
import org.example.task.mvvm.model.data.FilmDetails
import org.example.task.mvvm.model.data.SearchResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import retrofit2.http.Url


interface ImdbApi {
    @Headers(ApiConfig.HEADER_ACCEPT, ApiConfig.HEADER_CONTENT)
    @GET("/")
    fun searchFilms(@Query("apikey") apiKey: String, @Query("s") searchQuery: String): Call<SearchResult>

    @Headers(ApiConfig.HEADER_ACCEPT, ApiConfig.HEADER_CONTENT)
    @GET("/")
    fun filmDetails(@Query("apikey") apiKey: String, @Query("i") filmIdentifier: String): Call<FilmDetails>

    @GET
    fun downloadPoster(@Url url: String): Call<ResponseBody>
}