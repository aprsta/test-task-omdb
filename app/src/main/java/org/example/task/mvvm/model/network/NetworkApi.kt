package org.example.task.mvvm.model.network

import android.arch.lifecycle.MutableLiveData
import android.graphics.Bitmap
import android.util.Log
import okhttp3.ResponseBody
import org.example.task.common.config.ApiConfig
import org.example.task.mvvm.model.data.FilmDetails
import org.example.task.mvvm.model.data.SearchResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.graphics.BitmapFactory

class NetworkApi(private val imdbApi: ImdbApi) {
    companion object {
        private val TAG: String = NetworkApi::class.java.simpleName
    }

    fun asyncSearchFilms(searchQuery: String, liveData: MutableLiveData<SearchResult>) {
        imdbApi.searchFilms(ApiConfig.IMDB_API_KEY, searchQuery).enqueue(object : Callback<SearchResult> {
            override fun onResponse(call: Call<SearchResult>?, response: Response<SearchResult>?) {
                if (response == null || !response.isSuccessful || response.body() == null) {
                    Log.w(TAG, "[asyncSearchFilms] Error while searching for films. Request: '" + call?.request().toString() + "'. Code: '" + response?.code() + "'")

                    // TODO: Return error code in liveData to detail the error message.
                    liveData.postValue(null)
                    return
                }

                liveData.postValue(response.body())
            }

            override fun onFailure(call: Call<SearchResult>?, throwable: Throwable?) {
                Log.w(TAG, "[asyncSearchFilms] Failure while searching for films. Request: '" + call?.request().toString() + "'. Throws: '" + throwable?.message + "'")

                // TODO: Separate a NoConnection exception from others and return it in liveData.
                liveData.postValue(null)
            }
        })
    }

    fun asyncFilmDetails(filmIdentifier: String, liveData: MutableLiveData<FilmDetails>) {
        imdbApi.filmDetails(ApiConfig.IMDB_API_KEY, filmIdentifier).enqueue(object : Callback<FilmDetails> {
            override fun onResponse(call: Call<FilmDetails>?, response: Response<FilmDetails>?) {
                if (response == null || !response.isSuccessful || response.body() == null) {
                    Log.w(TAG, "[asyncFilmDetails] Error while getting film details. Request: '" + call?.request().toString() + "'. Code: '" + response?.code() + "'")

                    // TODO: Return error code in liveData to detail the error message.
                    liveData.postValue(null)
                    return
                }

                liveData.postValue(response.body())
            }

            override fun onFailure(call: Call<FilmDetails>?, throwable: Throwable?) {
                Log.w(TAG, "[asyncFilmDetails] Failure while getting film details. Request: '" + call?.request().toString() + "'. Throws: '" + throwable?.message + "'")

                // TODO: Separate a NoConnection exception from others and return it in liveData.
                liveData.postValue(null)
            }
        })
    }

    fun asyncDownloadPoster(url: String, liveData: MutableLiveData<Bitmap>) {
        imdbApi.downloadPoster(url).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response == null || !response.isSuccessful || response.body() == null) {
                    Log.w(TAG, "[asyncDownloadPoster] Error while downloading poster. Request: '" + call?.request().toString() + "'. Code: '" + response?.code() + "'")

                    // TODO: Return error code in liveData to detail the error message.
                    liveData.postValue(null)
                    return
                }

                val poster = BitmapFactory.decodeStream(response.body().byteStream())
                if (poster == null) {
                    Log.w(TAG, "[asyncDownloadPoster] Error while decoding stream. Request: '" + call?.request().toString() + "'. Code: '" + response.code() + "'")

                    // TODO: Return error code in liveData to detail the error message.
                    liveData.postValue(null)
                    return
                }

                liveData.postValue(poster)
            }

            override fun onFailure(call: Call<ResponseBody>?, throwable: Throwable?) {
                Log.w(TAG, "[asyncDownloadPoster] Failure while downloading poster. Request: '" + call?.request().toString() + "'. Throws: '" + throwable?.message + "'")

                // TODO: Separate a NoConnection exception from others and return it in liveData.
                liveData.postValue(null)
            }
        })
    }
}