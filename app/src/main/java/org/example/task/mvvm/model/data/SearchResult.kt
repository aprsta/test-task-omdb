package org.example.task.mvvm.model.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SearchResult(
        @SerializedName("Response")
        @Expose
        val response: String? = null,

        @SerializedName("Error")
        @Expose
        val error: String? = null,

        @SerializedName("totalResults")
        @Expose
        val total: String? = null,

        @SerializedName("Search")
        @Expose
        val search: List<SearchEntity>? = null
)