package org.example.task.mvvm.model.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class FilmDetails(
        @SerializedName("Title")
        @Expose
        val title: String? = null,

        @SerializedName("Poster")
        @Expose
        val posterUrl: String? = null,

        @SerializedName("imdbRating")
        @Expose
        val rating: String? = null,

        @SerializedName("Country")
        @Expose
        val country: String? = null,

        @SerializedName("Director")
        @Expose
        val director: String? = null,

        @SerializedName("Actors")
        @Expose
        val actors: String? = null,

        @SerializedName("Plot")
        @Expose
        val plot: String? = null
)