package org.example.task.mvvm.observable.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.content.Context
import android.databinding.Bindable
import android.support.v4.app.SupportActivity
import android.text.Editable
import android.text.TextWatcher
import org.example.task.Application
import org.example.task.BR
import org.example.task.R
import org.example.task.common.util.ObservableViewModel
import org.example.task.mvvm.model.data.SearchResult
import org.example.task.mvvm.model.network.NetworkApi
import org.example.task.mvvm.observable.baseobservable.SearchEntityBaseObservable
import javax.inject.Inject

class SearchViewModel : ObservableViewModel() {
    /** Section: Injects. */

    @Inject
    lateinit var networkApi: NetworkApi

    /** Section: Bindable properties. */

    var searchQuery: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.searchQuery)
        }

    var searchLoading: Boolean = false
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.searchLoading)
        }

    var searchError: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.searchError)
        }

    var searchResult: List<SearchEntityBaseObservable> = ArrayList()
        @Bindable get() = ArrayList(field)
        set(value) {
            field = ArrayList(value)
            notifyPropertyChanged(BR.searchResult)
        }

    /** Section: Live data. */

    // TODO: Modify liveData to return error details.
    private val searchLiveData = MutableLiveData<SearchResult>()

    /** Section: Constructor. */

    init {
        Application.component.inject(this)
    }

    /** Section: Utility methods. */

    fun observeAll(activity: SupportActivity) {
        searchLiveData.observe(activity, Observer {
            searchLoading = false

            if (it == null) {
                searchError = activity.getString(R.string.search_activity_error_message_network)
                return@Observer
            } else if (it.search == null) {
                searchError = activity.getString(R.string.search_activity_error_message_data)
                return@Observer
            }

            searchError = ""

            val result = it.search.map {
                val baseObservable = SearchEntityBaseObservable()
                baseObservable.searchEntityId = it.identifier.toString()
                baseObservable.searchEntityTitle = it.title.toString()
                return@map baseObservable
            }

            searchResult = result
        })
    }

    /** Section: Public methods. */

    fun search(context: Context) {
        if (searchQuery.isEmpty()) {
            searchError = context.getString(R.string.search_activity_error_message_empty)
            return
        }

        searchLoading = true
        searchError = ""

        networkApi.asyncSearchFilms(searchQuery, searchLiveData)
    }

    /** Section: Watchers. */

    val searchQueryChanged: TextWatcher = object : TextWatcher {
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun afterTextChanged(value: Editable) {
            searchQuery = value.toString()
        }
    }
}