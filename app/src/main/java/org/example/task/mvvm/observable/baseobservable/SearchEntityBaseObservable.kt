package org.example.task.mvvm.observable.baseobservable

import android.app.Activity
import android.content.Intent
import android.databinding.BaseObservable
import android.databinding.Bindable
import org.example.task.BR
import org.example.task.common.config.TransmittableKeys
import org.example.task.mvvm.view.activity.FilmDetailsActivity

class SearchEntityBaseObservable : BaseObservable() {
    /** Section: Bindable properties. */

    var searchEntityId: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.searchEntityId)
        }

    var searchEntityTitle: String = ""
        @Bindable get() = field
        set(value) {
            if (field == value) return
            field = value
            notifyPropertyChanged(BR.searchEntityTitle)
        }

    /** Section: Public methods. */

    fun details(activity: Activity) {
        if (searchEntityId.isEmpty()) {
            return
        }

        val intent = Intent(activity, FilmDetailsActivity::class.java)
        intent.putExtra(TransmittableKeys.IMDB_ID, searchEntityId)
        activity.startActivity(intent)
    }
}