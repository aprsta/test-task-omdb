package org.example.task.mvvm.view.activity

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import org.example.task.BR
import org.example.task.R
import org.example.task.databinding.ActivitySearchBinding
import org.example.task.mvvm.observable.viewmodel.SearchViewModel
import org.example.task.mvvm.view.adapter.SearchEntityAdapter
import java.lang.ref.WeakReference

class SearchActivity : AppCompatActivity() {
    private lateinit var viewModel: SearchViewModel
    private lateinit var searchEntityAdapter: SearchEntityAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivitySearchBinding>(this, R.layout.activity_search)
        viewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        binding.viewModel = viewModel

        val allCommentsRecyclerView = findViewById<RecyclerView>(R.id.searchEntityRecyclerView)
        searchEntityAdapter = SearchEntityAdapter()
        allCommentsRecyclerView.layoutManager = LinearLayoutManager(this)
        allCommentsRecyclerView.itemAnimator = DefaultItemAnimator()
        allCommentsRecyclerView.adapter = searchEntityAdapter

        val propertyHandler = PropertyHandler(this)
        viewModel.addOnPropertyChangedCallback(propertyHandler)

        viewModel.observeAll(this)
    }

    private class PropertyHandler(activity: SearchActivity) : Observable.OnPropertyChangedCallback() {
        val activity: WeakReference<SearchActivity> = WeakReference(activity)

        override fun onPropertyChanged(observable: Observable, propertyId: Int) {
            val activity = this.activity.get() ?: return

            if (propertyId == BR.searchResult) {
                activity.searchEntityAdapter.update(activity.viewModel.searchResult)
            }
        }
    }
}
