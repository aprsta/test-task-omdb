package org.example.task.common.di

import dagger.Component
import org.example.task.mvvm.observable.viewmodel.FilmDetailsViewModel
import org.example.task.mvvm.observable.viewmodel.SearchViewModel
import javax.inject.Singleton

@Component(modules = [NetworkModule::class, ContextModule::class])
@Singleton
interface Component {
    fun inject(viewModel: SearchViewModel)
    fun inject(viewModel: FilmDetailsViewModel)
}