package org.example.task.common.config

object ApiConfig {
    const val IMDB_API_PROTOCOL = "http"
    const val IMDB_API_HOST = "www.omdbapi.com"
    const val IMDB_API_PORT = 80
    const val IMDB_URL = "$IMDB_API_PROTOCOL://$IMDB_API_HOST:$IMDB_API_PORT"

    // FIXME: Get the real API key and move it to the xml file.
    const val IMDB_API_KEY = "509147ac"

    const val HEADER_ACCEPT = "Accept: application/json"
    const val HEADER_CONTENT = "Content-Type: application/json"

    const val TIMEOUT_MILLISEC: Long = 5000
    const val CACHE_SIZE: Long = 5 * 1024 * 1024
}